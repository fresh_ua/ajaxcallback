<?php

/**
  * @author Andrey Bon <andreybon1@gmail.com>
  * @license BSD-3
  * @package Artnet
  * @subpackage AjaxCallback
  * 
  * Module AjaxCallback for sending ajax-based form
  */

class Artnet_AjaxCallback_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction() // Main action
    {
        // is valid ajax request?
        if ($this->getRequest()->isXmlHttpRequest()) {
            // sanitize post data
            $name = filter_var($this->getRequest()->getPost('name'), FILTER_SANITIZE_SPECIAL_CHARS);
            $phone = filter_var($this->getRequest()->getPost('phone'), FILTER_SANITIZE_SPECIAL_CHARS);

            if (!empty($name) && !empty($phone)) // if data not empty
            {
                $tpl = Mage::getModel('core/email_template')->loadDefault("ajax_callback"); // load email template
                $data = array('aname'=>$name, 'aphone'=>$phone);
                $storeId = Mage::app()->getStore()->getId();

                $message = $tpl->getProcessedTemplate($data); // processing template
                // setting mail
                $tpl->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', $storeId));
                $tpl->setSenderName($name);
                $tpl->setTemplateSubject("Обратный звонок");

                // sending mail
                $send = $tpl->send(Mage::getStoreConfig('trans_email/ident_general/email', $storeId),
                                Mage::getStoreConfig('trans_email/ident_general/name', $storeId),
                                $data);
                
                if ($send):
                    echo json_encode(array("result"=>true));
                else:
                    $this->getResponse()->setHeader('HTTP/1.1', '503 Service Unavailable');

                    $this->getResponse()->sendHeadersAndExit();
                    echo json_encode(array("result"=>false));
                endif;
            }
            else // if data empty - send 503 header
            {
                $this->getResponse()->setHeader('HTTP/1.1', '503 Service Unavailable');
                $this->getResponse()->sendHeadersAndExit();
                echo json_encode(array("result"=>false));
            }
        }
        // if not - send 404 header
        else
        {
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');

            $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultNoRoute');
            }
        }
    }
} 