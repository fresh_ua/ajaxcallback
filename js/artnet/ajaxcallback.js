var aac = jQuery.noConflict();
var timeToClose = 2000;

aac(document).ready(function() {

    aac("#callback").bind("click", function(e) {
       aac("#ajax_overlay").show().animate({"opacity":1}, 500, "swing", function() {
          aac("#ajax_dialog").show();
       });
    });

    aac("#ajax_closer, #ajax_overlay").bind("click", function(e) {
        aac("#ajax_dialog").hide();
        aac("#ajax_overlay").animate({"opacity":0}, 500, function() {
            aac("#ajax_overlay").hide();
        });

    });

    aac("#ajax_dialog form > button[type='submit']").bind("click", function(e) {
        e.preventDefault();

        var form = aac("#ajax_dialog form");
        if (form.find("input").val().length>0)
        {
            aac.ajax({
                url: "/callback",
                data: form.serialize(),
                dataType: "JSON",
                type: "POST",
                success: function(jq, status, message)
                {
                    aac("#ajax_dialog").hide();
                    aac("#ajax_message").show().append("<p>Спасибо, Ваше запрос отправлен!</p>").append("<p>Менеджер свяжется с Вами в ближайшее время</p>");
                    setTimeout(function() {
                        aac("#ajax_message").hide();
                        aac("#ajax_message").find("p").remove();
                        aac("#ajax_overlay").animate({"opacity":0}, 500, function() {
                            aac("#ajax_overlay").hide();
                        });
                    }, timeToClose);
                },
                error: function(jq, status, message)
                {
                    aac("#ajax_dialog").hide();
                    aac("#ajax_message").show().append("<p>Произошла ошибка. Ваш запрос не отправлен.</p>");
                    setTimeout(function() {
                        aac("#ajax_message").hide();
                        aac("#ajax_message").find("p").remove();
                        aac("#ajax_overlay").animate({"opacity":0}, 500, function() {
                            aac("#ajax_overlay").hide();
                        });
                    }, timeToClose);
                }

            });
        }
    });
});